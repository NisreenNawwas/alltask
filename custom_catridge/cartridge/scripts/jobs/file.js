'use strict'

function execute() {
  var ProductMgr = require('dw/catalog/ProductMgr');
  var ContentMgr = require('dw/content/ContentMgr');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var product = 0;
  var name;
  var link;
  var productIterator = ProductMgr.queryAllSiteProducts();
  var con = ContentMgr.getContent("category-products");
  var products = [];


  while (productIterator.hasNext()) {
    product = productIterator.next();
    id = product.getID();
    link = URLUtils.https('Product-Show', 'pid', id).toString();
    products.push({
      ID: id,
      ProductLink: link
    });

  }
  Transaction.wrap(function () {
    con.getCustom().MyBody = JSON.stringify(products);
  });


}
exports.execute = execute;
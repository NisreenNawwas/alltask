
function allcurrency() {
    var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
    

    var service = LocalServiceRegistry.createService('allcurrency.http', {
        createRequest: function (svc, params) {
            svc.setURL('https://api.exchangeratesapi.io/latest');
            svc.setRequestMethod("GET");
            
      
        },
        parseResponse: function (svc, response) {
            return response;
        }
    }); 
    return service;
};
exports.allcurrency = allcurrency;


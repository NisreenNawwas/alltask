'use strict';
var server = require('server');
server.extend(module.superModule);



server.append('Show',function (req, res, next) {
    var sercall=require('*/cartridge/scripts/services/sercall.js');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr'); 
    var Transaction = require('dw/system/Transaction'); 
    var svc = sercall.allcurrency();
    var text= svc.call();
    var json = JSON.parse(text.object.text);
    var rateKey=Object.keys(json.rates); 
   
    for(var i = 0; i < 32; i++) {
        var obj = rateKey[i].toString();
        var rateValue=json.rates[obj];
        var cusObject =CustomObjectMgr.getCustomObject('exchangerates',obj);
        if(!cusObject){
        Transaction.begin();  
        var customObject = CustomObjectMgr.createCustomObject('exchangerates', obj);
        customObject.custom.currency = obj;
        customObject.custom.exchangevalue = rateValue;
        Transaction.commit();  
        }
        else{
            Transaction.begin();  
            cusObject.custom.currency = obj;
            cusObject.custom.exchangevalue  = rateValue;
            Transaction.commit(); 
        }
    }
  
    
   
    var ContentMgr = require('dw/content/ContentMgr');
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var con = ContentMgr.getContent("category-products");
    
    var catId= req.querystring.cgid;
    var siteCat= CatalogMgr.getCategory(catId);
    var catName = siteCat.getDisplayName();

    var str =  con.getCustom().MyBody;
    var json = JSON.parse(str);
    var products=[];
    for(var i = 0; i < json.length; i++) {
        var obj = json[i];
        var product = ProductMgr.getProduct((obj.ID).toString());
        if (product.getCategories().contains(siteCat)) {
            products.push(product);
        }
        
    }
    
    var view = {
        products: products,
        catId: req.querystring.cgid,
        catName: catName
    };
    res.setViewData(view);
    next();
});

module.exports = server.exports();

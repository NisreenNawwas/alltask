'use strict';
var server = require('server');
server.extend(module.superModule);
var OrderMgr = require('dw/order/OrderMgr');
var Transaction = require('dw/system/Transaction');



server.append('PlaceOrder', server.middleware.https, function (req, res, next) {
   
    var viewData = res.getViewData();
    var curOrder = OrderMgr.getOrder(viewData.orderID);
    Transaction.wrap(function () {
        var counter = curOrder.getProductQuantityTotal();
        curOrder.custom.productcount = counter;
    });

    next();  
});
module.exports = server.exports();


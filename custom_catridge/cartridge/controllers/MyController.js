'use strict';
var server = require('server');
var URLUtils = require('dw/web/URLUtils');
var OrderMgr = require('dw/order/OrderMgr');
var Transaction = require('dw/system/Transaction');
server.get('Show', function (req, res, next) {
    var actionUrl = URLUtils.url('MyController-handleForm'); 
    res.render('test/page1', {
        actionUrl: actionUrl
   
    });
next();
});


server.post('handleForm', function (req, res, next) {
    var number = req.form.number;
    var order = OrderMgr.getOrder(number);
    var status=order.getStatus();
    var price=order.getTotalNetPrice();
    var count=order.getCustom().counter == null ? 0 : order.getCustom().counter;
    Transaction.wrap(function(){
        order.getCustom().counter = count + 1;
    });
    count=order.getCustom().counter;
    res.render('test/page2', {
        number : number,
        count:count,
        status:status,
        price:price
    });

    next();
    });
    
    module.exports = server.exports();

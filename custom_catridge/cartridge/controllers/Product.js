'use strict';
var server = require('server');


server.get('ShowProduct', function (req, res, next) {
    var HookMgr = require('dw/system/HookMgr');
    var id=req.querystring.id;
  
    var productName=HookMgr.callHook('dw.ocapi.shop.product.modifyGETResponse', 'modifyGETResponse',id.toString());

    res.json({
        productName:productName
    });
 
next();
});
module.exports = server.exports();